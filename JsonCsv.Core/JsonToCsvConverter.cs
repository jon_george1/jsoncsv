﻿namespace JsonCsv.Core
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class JsonToCsvConverter
    {
        private readonly CsvOptions options;

        private readonly string rowTemplate;

        public JsonToCsvConverter(CsvOptions options = null)
        {
            this.options = options ?? CsvOptions.Default;
            this.rowTemplate = this.options.QuoteValues
                                   ? string.Concat("\"{0}\"", this.options.ColumnDelimiter, "\"{1}\"")
                                   : string.Concat("{0}", this.options.ColumnDelimiter, "{1}");
        }

        public void Convert(string inputFile, string outputFile)
        {
            var input = File.OpenRead(inputFile);
            var output = File.OpenWrite(outputFile);
            this.Convert(input, output);
        }

        public void Convert(Stream input, Stream output)
        {
            using (var streamReader = new StreamReader(input))
            {
                using (var jsonReader = new JsonTextReader(streamReader))
                {
                    using (var writer = new StreamWriter(output))
                    {
                        var root = JObject.Load(jsonReader);

                        this.ProcessChild(new string[0], root, writer);
                    }
                }
            }
        }

        public void ProcessChild(IEnumerable<string> path, JToken current, StreamWriter output)
        {
            if (current is JProperty)
            {
                var jpropertyCurrent = current as JProperty;

                if (jpropertyCurrent.Value is JObject)
                {
                    this.ProcessChild(path.Concat(new[] { jpropertyCurrent.Name }), jpropertyCurrent.Value, output);
                }
                else
                {
                    var fieldName = string.Concat(string.Join(".", path), ".", jpropertyCurrent.Name);
                    output.WriteLine(this.rowTemplate, fieldName, this.options.Escape((string)jpropertyCurrent.Value));
                }
            }
            else
            {
                foreach (var child in current)
                {
                    this.ProcessChild(path, child, output);
                }
            }
        }
    }
}