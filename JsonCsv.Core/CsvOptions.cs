﻿namespace JsonCsv.Core
{
    using System;

    public class CsvOptions
    {
        public static CsvOptions Default = new CsvOptions
                                                     {
                                                         EscapeStrings = new[]
                                                             {
                                                                 new Tuple<string, string>("\"", "\"\""), 
                                                             }
                                                     };

        public CsvOptions()
        {
            this.NamesColumn = 0;
            this.ValuesColumn = 1;
            this.ColumnDelimiter = ",";
            this.QuoteValues = true;
        }

        public int NamesColumn { get; set; }

        public int ValuesColumn { get; set; }

        public string ColumnDelimiter { get; set; }

        public bool QuoteValues { get; set; }

        public Tuple<string, string>[] EscapeStrings { get; set; }

        public string Escape(string input)
        {
            var output = input;

            foreach (var current in this.EscapeStrings)
            {
                output = output.Replace(current.Item1, current.Item2);
            }

            return output;
        }
    }
}