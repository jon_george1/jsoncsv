﻿namespace JsonCsv.Core
{
    using System.Collections.Generic;
    using System.Dynamic;
    using System.IO;

    using Microsoft.VisualBasic.FileIO;

    using Newtonsoft.Json;

    public class CsvToJsonConverter
    {
        private readonly CsvOptions options;

        public CsvToJsonConverter(CsvOptions options = null)
        {
            this.options = options ?? CsvOptions.Default;
        }

        public void Convert(string inputFile, string outputFile)
        {
            var input = File.OpenRead(inputFile);
            var output = File.OpenWrite(outputFile);
            this.Convert(input, output);
        }

        public void Convert(Stream input, Stream output)
        {
            var result = new ExpandoObject();

            using (var reader = new TextFieldParser(input))
            {
                reader.HasFieldsEnclosedInQuotes = this.options.QuoteValues;
                reader.SetDelimiters(this.options.ColumnDelimiter);

                while (!reader.EndOfData)
                {
                    var line = reader.ReadFields();

                    this.SetValue(result, line[this.options.NamesColumn], line[this.options.ValuesColumn]);
                }
            }

            var json = JsonConvert.SerializeObject(result, Formatting.Indented);
            
            using (var writer = new StreamWriter(output))
            {
                writer.Write(json);
            }
        }

        private void SetValue(IDictionary<string, object> target, string propertyName, string value)
        {
            var firstDot = propertyName.IndexOf(".");

            if (firstDot == -1)
            {
                target.Add(propertyName, value);
            } 
            else
            {
                var currentPropertyName = propertyName.Substring(0, firstDot);
                var remainingPropertyName = propertyName.Substring(firstDot + 1);

                ExpandoObject nextTarget;
                
                if (target.ContainsKey(currentPropertyName))
                {
                    nextTarget = (ExpandoObject)target[currentPropertyName];
                }
                else
                {
                    nextTarget = new ExpandoObject();
                    target[currentPropertyName] = nextTarget;
                }

                this.SetValue(nextTarget, remainingPropertyName, value);
            }
        }
    }
}