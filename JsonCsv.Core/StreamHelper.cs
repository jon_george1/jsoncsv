﻿namespace JsonCsv.Core
{
    using System;
    using System.IO;
    using System.Net;

    public static class StreamHelper
    {
        public static Stream GetInputStreamFor(string path)
        {
            return GetStreamFor(path, false);
        }

        public static Stream GetOutputStreamFor(string path)
        {
            return GetStreamFor(path, true);
        }

        private static Stream GetStreamFor(string path, bool writeable)
        {
            try
            {
                var uri = new Uri(path);

                if (uri.IsFile)
                {
                    return File.Open(path, writeable ? FileMode.Create : FileMode.Open);
                }

                // Treat as web
                if (writeable)
                {
                    throw new Exception("Cannot make a web location writable");
                }

                return WebRequest.Create(uri).GetResponse().GetResponseStream();
            }
            catch (UriFormatException)
            {
                // Assume it's a relative file path
                return File.Open(path, writeable ? FileMode.Create : FileMode.Open);
            }
        }
    }
}