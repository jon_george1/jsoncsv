﻿namespace JsonToCsv
{
    using CommandLine;
    using CommandLine.Text;

    public class Options
    {
        [Option('i', "input", Required = true, HelpText = "Input file to be processed")]
        public string InputFile { get; set; }

        [Option('o', "output", Required = false, DefaultValue = "output.csv", HelpText = "Path to save the results to")]
        public string OutputFile { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}