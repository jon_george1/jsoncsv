﻿namespace JsonToCsv
{
    using System;

    using JsonCsv.Core;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                var converter = new JsonToCsvConverter();

                converter.Convert(StreamHelper.GetInputStreamFor(options.InputFile), StreamHelper.GetOutputStreamFor(options.OutputFile));
            }

            Console.WriteLine("Press enter to finish");
            Console.ReadLine();
        }
    }
}