﻿namespace JsonCsv.MsBuild
{
    using JsonCsv.Core;

    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;

    public class CsvToJson : Task
    {
        public CsvToJson()
        {
            this.NamesColumn = 0;
            this.ValuesColumn = 1;
            this.ColumnDelimiter = ",";
            this.QuoteValues = true;
        }

        [Required]
        public string InputFile { get; set; }

        [Required]
        public string OutputFile { get; set; }

        public int NamesColumn { get; set; }

        public int ValuesColumn { get; set; }

        public string ColumnDelimiter { get; set; }

        public bool QuoteValues { get; set; }

        public override bool Execute()
        {
            var csvOptions = this.BuildCsvOptions();

            using (var inputStream = StreamHelper.GetInputStreamFor(this.InputFile))
            {
                using (var outputStream = StreamHelper.GetOutputStreamFor(this.OutputFile))
                {
                    var converter = new CsvToJsonConverter(csvOptions);

                    converter.Convert(inputStream, outputStream);
                }
            }

            return true;
        }

        private CsvOptions BuildCsvOptions()
        {
            return new CsvOptions
                {
                    NamesColumn = this.NamesColumn,
                    ValuesColumn = this.ValuesColumn,
                    QuoteValues = this.QuoteValues,
                    ColumnDelimiter = this.ColumnDelimiter
                };
        }
    }
}