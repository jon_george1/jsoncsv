﻿namespace CsvToJson
{
    using System;
    using System.IO;
    using System.Net;

    using JsonCsv.Core;

    internal class Program
    {
        private static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                var csvOptions = new CsvOptions
                    {
                        EscapeStrings = CsvOptions.Default.EscapeStrings,
                        NamesColumn = options.NamesColumn,
                        ValuesColumn = options.ValuesColumn
                    };

                var converter = new CsvToJsonConverter(csvOptions);

                converter.Convert(StreamHelper.GetInputStreamFor(options.InputFile), StreamHelper.GetOutputStreamFor(options.OutputFile));
            }

            Console.WriteLine("Press enter to finish");
            Console.ReadLine();
        }
    }
}