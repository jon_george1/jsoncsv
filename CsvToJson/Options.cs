﻿namespace CsvToJson
{
    using CommandLine;
    using CommandLine.Text;

    public class Options
    {
        [Option('i', "input", Required = true, HelpText = "Input file to be processed")]
        public string InputFile { get; set; }

        [Option('o', "output", Required = false, DefaultValue = "output.json", HelpText = "Path to save the results to")]
        public string OutputFile { get; set; }

        [Option('n', "names", Required = false, DefaultValue = 0, HelpText = "Column that contains the object names")]
        public int NamesColumn { get; set; }

        [Option('v', "values", Required = false, DefaultValue = 1, HelpText = "Column that contains the object values")]
        public int ValuesColumn { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}